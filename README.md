Использовался метод многоэтапной сборки с официальной инструкций. 
https://docs.docker.com/build/building/multi-stage/

Из образа maven собирается само приложение, а потом это приложение копируется на другой образ и запускается уже в нём.
Docker build -t name . из директорий с файлами.

Дальше запускается контейнер с помощью Docker run -p 8080:8080 name

Размер образа вышел в 346MB. 

Dockerfile

FROM maven:3.6.3-openjdk-17 AS build  
COPY src /home/app/src    
COPY pom.xml /home/app   
RUN mvn -f /home/app/pom.xml clean package  


FROM openjdk:17-jdk-alpine  
COPY --from=build /home/app/target/hallo-0.0.1-SNAPSHOT.jar /usr/local/lib/hallo-0.0.1-SNAPSHOT.jar  
EXPOSE 8080   
ENTRYPOINT ["java","-jar","/usr/local/lib/hallo-0.0.1-SNAPSHOT.jar"]  

___________________________________________________________________________________


Используя утилиту jdeps и jlink удалось определить зависимости для запуска приложения.   
Таким образом можно собрать свой образ только с нужными зависимостями для запуска java приложения.  

jlink --no-header-files --no-man-pages --compress=2 --strip-java-debug-attributes --add-modules java.base,java.desktop,java.logging,java.management,java.naming,java.security.jgss,java.instrument --output slim-jre7  

При остутствий какой либо зависимости приложение не запустится с ошибкой отсутствия модуля.  
Error: Module java.security.jqss not found  


Зависимости ( java.base,java.desktop,java.logging,java.management,java.naming,java.security.jgss,java.instrument )  

Проверка запуска приложения выполнилась успешно.  
slim-jre7\bin\java -jar target\hallo-0.0.1-SNAPSHOT.jar  

Нашёл статью по созданию докерфайла только с нужными зависимостями под java.
https://dou.ua/lenta/articles/slim-docker-image/ 
Смысла нет изобретать велосипед) 

В итоге удалось собрать свой образ размером 160 мб и запустить приложение используя этот образ.

Для сборки образа только с определенным зависимостями.
docker build -t custom .

Ну и на его основе запускаем своё приложение с копированием и запуском java.







